import { writeFileSync } from "fs";
import path from "path";
import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Sequelize,
  Table,
} from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { NUMBER_1, NUMBER_2 } = process.env;

const task1 = () => {
  const number1Parse = Number(NUMBER_1);
  const number2Parse = Number(NUMBER_2);
  if (isNaN(number1Parse)) {
    writeFileSync(
      filePathForTask1,
      JSON.stringify({
        message: "Агрумент NUMBER_1 не был передан или некорректный",
      })
    );
    return;
  }

  if (isNaN(number2Parse)) {
    writeFileSync(
      filePathForTask1,
      JSON.stringify({
        message: "Агрумент NUMBER_2 не был передан или некорректный",
      })
    );
    return;
  }

  const number1ABS = Math.abs(number1Parse);
  const number2ABS = Math.abs(number2Parse);

  const number1String = number1ABS.toString();
  const number2String = number2ABS.toString();

  let result = 0;
  let message = "";
  if (number1String.length > number2String.length) {
    result = number1ABS;
    message = `В числе ${NUMBER_1} больше цифр чем в числе ${NUMBER_2}`;
  } else if (number1String.length === number2String.length) {
    message = `В числе ${NUMBER_1} столько же цифр, сколько в числе ${NUMBER_2}`;
  } else {
    result = number2ABS;
    message = `В числе ${NUMBER_2} больше цифр чем в числе ${NUMBER_1}`;
  }

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        NUMBER_1,
        NUMBER_2,
      },
      result,
      message,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Mark extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    lesson: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    fio: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    examDate: string;

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    mark: number;

    @ForeignKey(() => Student)
    studentId: string;

    @BelongsTo(() => Student, {
      foreignKey: "studentId",
    })
    student!: Student;
  }

  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;

    @HasMany(() => Mark, {
      foreignKey: "studentId",
    })
    markList: Mark[];
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student, Mark]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  //Ищем всех студентов у кого есть 2 в оценках
  const studentList = await Student.findAll({
    include: [
      {
        model: Mark,
        required: true,
        where: {
          mark: 2,
        },
      },
    ],
  });

  //Маппинг студентов в правильный формат
  const mappedStudentList = studentList.map((el) => {
    return {
      surname: el.dataValues["surname"],
      name: el.dataValues["name"],
    };
  });
  writeFileSync(filePathForTask2, JSON.stringify(mappedStudentList));
};

initDB();
